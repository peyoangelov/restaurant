# Peyo Angelov's exam module_01 repository.

# Scale Camp
![N|Solid](http://iteventz.bg/images/upload/8ea09dd1e475560c17ffd16f737ce7bc.jpg)

# Peyo Angelov's repository.
## Module 1:
Scale camp modules:
  - Indoruction to Java
  - OOP in Java
  - Unit Test
  - Exceptions and exceptions hangling.
  - Data structute
  - Files Input/Output streams.
  - Design patterns
  - Logging and debugging
  - High quality code
