import entities.Client;
import entities.Restaurant;
import entities.Waiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.impl.RestaurantCreationService;
import services.impl.RestaurantSimulationService;

import java.io.IOException;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        final Logger LOGGER = LoggerFactory.getLogger(Demo.class);


        final RestaurantCreationService restaurantCreationService = new RestaurantCreationService();
        final RestaurantSimulationService restaurantSimulationService = new RestaurantSimulationService();

        final Restaurant restaurant = restaurantCreationService.createRestaurant("3 little pigs", " Sofia ");
        final List<Client> clients = restaurantCreationService.createClients();

        try {
            restaurantSimulationService.simulate(clients, restaurant);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("The Bill was not generated");
        } catch
        (Exception e) {
            e.printStackTrace();
        }

        LOGGER.info("Money after the simulation: " + restaurant.getMoney());

        final Waiter waiter = restaurantSimulationService.getWaiterWithHighestTipAmount(restaurant);
        if (waiter != null) {
            LOGGER.info("The waiter with highest tip is  " + waiter.getName() + " with tip amount " + waiter.getMoney());
        }
        final List<Waiter> waiters = restaurant.getAllWaiters();
        for (Waiter w : waiters) {
            LOGGER.info("The waiter: " + w.getName() + " with tip amount " + w.getMoney());

        }
        restaurantSimulationService.showLeftMenuItems(restaurant);

    }
}
