package constants;

import java.math.BigDecimal;

public interface RestaurantConstants {
    BigDecimal WAITERS_INITIAL_MONEY = new BigDecimal(10);
    BigDecimal STUDENT_CLIENT_INITIAL_MONEY = new BigDecimal(15);
    BigDecimal VEGAN_CLIENT_INITIAL_MONEY = new BigDecimal(30);
    BigDecimal REGULAR_CLIENT_INITIAL_MONEY = new BigDecimal(50);
    BigDecimal RESTAURANT_INITIAL_MONEY = new BigDecimal(1000);
    BigDecimal SALAD_PRICE = new BigDecimal(6);
    BigDecimal SALAD_GRAMS = new BigDecimal(500);
    BigDecimal DESERT_PRICE = new BigDecimal(5);
    BigDecimal DESERT_GRAMS = new BigDecimal(200);
    BigDecimal MAIN_PRICE = new BigDecimal(10);
    BigDecimal MAIN_GRAMS = new BigDecimal(700);

    String SALAD_NAME = "SHOPSKA";
    String DESERT_NAME = "Cake";
    String MAIN_NAME = "Musaka";

    String DRINK_ALCOHOL_NAME = "Vodka";
    String DRINK_NON_ALCOHOL_NAME = "Fanta";
    BigDecimal DRINK_PRICE_ALCOHOL = new BigDecimal(4);
    BigDecimal DRINK_PRICE_NON_ALCOHOL = new BigDecimal(2);

    int MAX_NUMBER_OF_CLIENTS = 15;
    int MAX_NUMBER_OF_DRINKS = 20;
    int MAX_NUMBER_OF_MEALS = 15;
    int MAX_NUMBER_OF_WAITERS = 6;
    int MAX_NUMBER_OF_PRODUCTS_TO_ORDER = 2;
    int STARTING_POSITION = 1;
    double MAX_PERCENTAGE_OF_ALLOW_ORDER_COST = 0.9;
    double CHANCE_OF_OCCURRENCE_20 = 0.2;
    double CHANCE_OF_OCCURRENCE_50 = 0.5;
}
