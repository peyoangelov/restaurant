package entities;

import java.math.BigDecimal;

public class Drink extends StockPriceName implements Comparable {
    private boolean isAlcohol;


    public Drink(String name, BigDecimal price, boolean isAlcohol) {
        super(price, name);
        this.setAlcohol(isAlcohol);
    }

    public boolean isAlcohol() {
        return isAlcohol;
    }

    public void setAlcohol(boolean alcohol) {
        isAlcohol = alcohol;
    }

    @Override
    public int compareTo(Object o) {
        return Boolean.valueOf(((Drink) o).isAlcohol).compareTo(Boolean.valueOf(this.isAlcohol));
    }
}
