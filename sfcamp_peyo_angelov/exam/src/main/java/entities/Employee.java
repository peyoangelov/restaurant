package entities;

import java.math.BigDecimal;

public abstract class Employee {
    private String name;
    private BigDecimal money;


    public Employee(String name, BigDecimal money) {
        this.setName(name);
        this.setMoney(money);
    }

    public void addMoney(final BigDecimal money) {
        this.money = this.money.add(money);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }


}
