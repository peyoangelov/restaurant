package entities;

import enums.MealType;

import java.math.BigDecimal;

public class Meal extends StockPriceName implements Comparable {
    private MealType mealType;
    private BigDecimal grams;


    public Meal(String name, BigDecimal price, MealType mealType, BigDecimal grams) {
        super(price, name);
        this.setMealType(mealType);
        this.setGrams(grams);

    }

    public BigDecimal getGrams() {
        return grams;
    }

    public void setGrams(BigDecimal grams) {
        this.grams = grams;
    }

    public MealType getMealType() {
        return mealType;
    }

    public void setMealType(MealType mealType) {
        this.mealType = mealType;
    }


    @Override
    public int compareTo(Object o) {

        return ((Meal) o).getMealType().compareTo(this.getMealType());

    }
}
