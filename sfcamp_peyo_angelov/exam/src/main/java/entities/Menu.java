package entities;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    List<Meal> meals = new ArrayList<Meal>();
    List<Drink> drinks = new ArrayList<Drink>();


    public void addMealToMenu(final Meal meal) {
        meals.add(meal);
    }

    public void removeMeal(final Meal meal) {
        meals.remove(meal);
    }

    public void addDrinkToMenu(final Drink drink) {
        drinks.add(drink);
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public void removeDrink(final Drink drink) {
        this.drinks.remove(drink);
    }
}
