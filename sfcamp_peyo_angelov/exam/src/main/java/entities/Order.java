package entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Meal> meals = new ArrayList<Meal>();
    private List<Drink> drinks = new ArrayList<Drink>();
    private BigDecimal price;
    private Client client;

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Order(List<Meal> meals, List<Drink> drinks, BigDecimal price, Client client) {
        this.meals = meals;
        this.drinks = drinks;
        this.price = price;
        this.client = client;
    }

    public Order(Client client) {
        this.client = client;
    }

    public void addMeal(final Meal meal) {
        meals.add(meal);
    }

    public void addDrink(final Drink drink) {
        drinks.add(drink);
    }

    public Client getClient() {
        return client;
    }


    public void calculateThePrice() {
        BigDecimal price = new BigDecimal(0);
        for (Meal m : this.meals) {
            price = price.add(m.getPrice());
        }
        for (Drink d : this.drinks) {
            price = price.add(d.getPrice());
        }
        this.setPrice(price);

    }
}
