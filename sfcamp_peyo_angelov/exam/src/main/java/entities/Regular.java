package entities;

import static constants.RestaurantConstants.REGULAR_CLIENT_INITIAL_MONEY;

public class Regular extends Client {

    public Regular(String name) {
        super(name, REGULAR_CLIENT_INITIAL_MONEY);
    }
}
