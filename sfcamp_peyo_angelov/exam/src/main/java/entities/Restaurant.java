package entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Restaurant {
    private static Restaurant SINGLE_RESTAURANT_INSTANCE = null;

    private String name;
    private String address;
    private BigDecimal money;
    private Menu menu;
    private final List<Waiter> waiters = new ArrayList();


    /**
     * static method to create instance of Singleton class
     */
    public static Restaurant getInstance() {
        if (SINGLE_RESTAURANT_INSTANCE == null)
            SINGLE_RESTAURANT_INSTANCE = new Restaurant();

        return SINGLE_RESTAURANT_INSTANCE;
    }

    /**
     * Adding a waiter to the collection
     */
    public void addWaiter(final Waiter newWaiter) {
        if (newWaiter != null)
            waiters.add(newWaiter);
    }

    /**
     * Return the collection of all waiters, without change it
     *
     * @return
     */
    public List<Waiter> getAllWaiters() {
        Collections.sort(waiters);
        return Collections.unmodifiableList(waiters);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void addMoney(final BigDecimal money) {
        this.money = this.money.add(money);
    }
}
