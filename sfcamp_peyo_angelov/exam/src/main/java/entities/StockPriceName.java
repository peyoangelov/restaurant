package entities;

import java.math.BigDecimal;

public abstract class StockPriceName {
    private BigDecimal price;
    private String name;


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public StockPriceName(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StockPriceName(BigDecimal price, String name) {
        this.price = price;
        this.name = name;
    }


}
