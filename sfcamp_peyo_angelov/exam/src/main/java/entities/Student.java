package entities;

import static constants.RestaurantConstants.STUDENT_CLIENT_INITIAL_MONEY;

public class Student extends Client {

    public Student(String name) {
        super(name, STUDENT_CLIENT_INITIAL_MONEY);
    }
}
