package entities;

import static constants.RestaurantConstants.VEGAN_CLIENT_INITIAL_MONEY;

public class Vegan extends Client {

    public Vegan(String name) {
        super(name, VEGAN_CLIENT_INITIAL_MONEY);
    }
}
