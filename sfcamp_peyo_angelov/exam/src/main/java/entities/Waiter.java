package entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Waiter extends Employee implements Comparable {
    private List<Order> orders;

    public Waiter(String name, BigDecimal money) {
        super(name, money);
        orders = new ArrayList();
    }

    public void addOrder(final Order order) {
        orders.add(order);
    }

    public List<Order> getOrders() {
        return orders;
    }


    @Override
    public int compareTo(Object o) {
        return ((Waiter) o).getMoney().compareTo(this.getMoney());
    }
}
