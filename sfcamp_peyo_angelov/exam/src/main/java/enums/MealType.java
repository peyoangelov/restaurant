package enums;

public enum MealType {
    MAIN_MEAL,
    SALADS,
    DESERTS
}
