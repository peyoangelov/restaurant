package services;

import entities.Client;
import entities.Restaurant;

import java.util.List;

public interface RestaurantCreationServiceInterface {
    Restaurant createRestaurant(final String name, final String address);

    List<Client> createClients();
}
