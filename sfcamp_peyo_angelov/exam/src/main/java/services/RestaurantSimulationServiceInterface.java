package services;

import entities.Client;
import entities.Restaurant;
import entities.Waiter;

import java.io.IOException;
import java.util.List;

public interface RestaurantSimulationServiceInterface {
    void simulate(final List<Client> customers, final Restaurant res) throws IOException;

    Waiter getWaiterWithHighestTipAmount(final Restaurant restaurant);

    void showLeftMenuItems(final Restaurant restaurant);

}

