package services.impl;

import entities.*;
import enums.MealType;
import services.RestaurantCreationServiceInterface;

import java.util.ArrayList;
import java.util.List;

import static constants.RestaurantConstants.*;


public class RestaurantCreationService implements RestaurantCreationServiceInterface {
    /**
     * creating the restaurant
     *
     * @param name    of the restaurant
     * @param address of the restaurant
     * @return restaurant
     */
    @Override
    public Restaurant createRestaurant(final String name, final String address) {
        final Restaurant restaurant = Restaurant.getInstance();
        restaurant.setName(name);
        restaurant.setAddress(address);
        restaurant.setMoney(RESTAURANT_INITIAL_MONEY);
        restaurant.setMenu(new Menu());
        fillTheRestaurantWithWaiters(restaurant);
        fillWithMenu(restaurant);
        return restaurant;
    }

    /**
     * Create different type of clients
     *
     * @return List of clients
     */
    @Override
    public List<Client> createClients() {
        List<Client> clients = new ArrayList();

        for (int i = STARTING_POSITION; i <= MAX_NUMBER_OF_CLIENTS; i++) {
            double chance = Math.random();
            if (chance <= CHANCE_OF_OCCURRENCE_20) {
                clients.add(createClientVegan("Vegan_" + i));
            } else if (chance > CHANCE_OF_OCCURRENCE_20 && chance <= CHANCE_OF_OCCURRENCE_50) {
                clients.add(createClientStudent("Student_" + i));
            } else {
                clients.add(createClientRegular("Regular_" + i));
            }
        }
        return clients;
    }

    private Meal createMealSalad() {
        return new Meal(SALAD_NAME, SALAD_PRICE, MealType.SALADS, SALAD_GRAMS);
    }

    private Meal createMealDesert() {
        return new Meal(DESERT_NAME, DESERT_PRICE, MealType.DESERTS, DESERT_GRAMS);
    }

    private Meal createMealMain() {
        return new Meal(MAIN_NAME, MAIN_PRICE, MealType.MAIN_MEAL, MAIN_GRAMS);
    }

    private Drink createAlcoholicDrink() {
        return new Drink(DRINK_ALCOHOL_NAME, DRINK_PRICE_ALCOHOL, true);
    }

    private Drink createNONAlcoholicDrink() {
        return new Drink(DRINK_NON_ALCOHOL_NAME, DRINK_PRICE_NON_ALCOHOL, false);
    }

    private Waiter createWaiter(String name) {
        Waiter waiter1 = new Waiter(name, WAITERS_INITIAL_MONEY);
        return waiter1;
    }

    private Client createClientVegan(String name) {
        return new Vegan(name);
    }

    private Client createClientStudent(String name) {
        return new Student(name);
    }

    private Client createClientRegular(String name) {
        return new Regular(name);
    }

    private void fillWithMenu(Restaurant restaurant) {
        createDrinks(restaurant);
        createMeals(restaurant);
    }

    private void createDrinks(Restaurant restaurant) {
        for (int i = STARTING_POSITION; i <= MAX_NUMBER_OF_DRINKS; i++) {
            restaurant.getMenu().addDrinkToMenu(createAlcoholicDrink());
            restaurant.getMenu().addDrinkToMenu(createNONAlcoholicDrink());
        }
    }

    private void createMeals(Restaurant restaurant) {
        for (int i = STARTING_POSITION; i <= MAX_NUMBER_OF_MEALS; i++) {
            restaurant.getMenu().addMealToMenu(createMealDesert());
            restaurant.getMenu().addMealToMenu(createMealMain());
            restaurant.getMenu().addMealToMenu(createMealSalad());
        }
    }

    private Restaurant fillTheRestaurantWithWaiters(Restaurant restaurant) {

        for (int i = STARTING_POSITION; i < MAX_NUMBER_OF_WAITERS; i++) {
            restaurant.addWaiter(createWaiter("Waiter_" + i));
        }
        return restaurant;
    }


}


