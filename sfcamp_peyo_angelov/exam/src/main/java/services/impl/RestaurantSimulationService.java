package services.impl;

import entities.*;
import enums.MealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.RestaurantSimulationServiceInterface;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import static constants.RestaurantConstants.MAX_NUMBER_OF_PRODUCTS_TO_ORDER;
import static constants.RestaurantConstants.MAX_PERCENTAGE_OF_ALLOW_ORDER_COST;

public class RestaurantSimulationService implements RestaurantSimulationServiceInterface {
    final Logger LOGGER = LoggerFactory.getLogger(RestaurantSimulationService.class);

    /**
     * Simulate the work of the restaurant, make order, ask for the bill, pay the bill
     *
     * @param customers in the restaurant
     * @param res       is the restaurant
     * @throws IOException
     */
    @Override
    public void simulate(final List<Client> customers, final Restaurant res) throws IOException {
        LOGGER.info("Customers are here");
        final Waiter waiter = getRandomWaiter(res.getAllWaiters());
        for (Client cus : customers) {
            makeOrder(cus, res, waiter);
            askTheBill(cus, res);
            payTheBill(cus, res);
        }
    }


    @Override
    public Waiter getWaiterWithHighestTipAmount(final Restaurant restaurant) {
        BigDecimal maxTip = new BigDecimal(0);
        Waiter waiterWithHighestTip = null;
        for (Waiter w : restaurant.getAllWaiters()) {
            if (w.getMoney().compareTo(maxTip) == 1) {
                maxTip = w.getMoney();
                waiterWithHighestTip = w;
            }
        }
        return waiterWithHighestTip;
    }

    @Override
    public void showLeftMenuItems(Restaurant restaurant) {
        final List<Meal> meals = restaurant.getMenu().getMeals();
        final List<Drink> drinks = restaurant.getMenu().getDrinks();
        LOGGER.info("Menu items left after the simulation: ");
        if (meals.isEmpty()) {
            LOGGER.info("There are no meals left!");
        } else {
            for (MealType mealType : MealType.values()) {
                final List<Meal> food = meals.stream()
                        .filter(m -> m.getMealType().equals(mealType))
                        .collect(Collectors.toList());
                LOGGER.info(mealType + " " + food.size());
            }
        }
        if (drinks.isEmpty()) {
            LOGGER.info("There are no drinks left!");
        } else {
            final List<Drink> alcohol = drinks.stream().filter(d -> d.isAlcohol() == true).collect(Collectors.toList());
            LOGGER.info("Alcohol: " + alcohol.size());
            final List<Drink> nonAlcohol = drinks.stream().filter(d -> d.isAlcohol() == false).collect(Collectors.toList());
            LOGGER.info("NON Alcohol: " + nonAlcohol.size());
        }
    }


    private void makeVeganOrder(final Client client, final Menu menu, final Waiter waiter) {
        final Order order = new Order(client);
        Meal meal;
        Drink drink;
        BigDecimal moneyLeft = client.getMoney();
        int countNumberOfProductsOrdered = 0;
        // get random meal from menu meals

        while (countNumberOfProductsOrdered <= MAX_NUMBER_OF_PRODUCTS_TO_ORDER && client.getMoney().subtract(moneyLeft).divide(client.getMoney(), 1, RoundingMode.HALF_EVEN).compareTo(new BigDecimal(0.9)) == -1) {

            if (getRandomBoolean()) {

                Optional<Meal> mealOptional = menu.getMeals()
                        .stream()
                        .filter(m -> m.getMealType().equals(MealType.SALADS))
                        .findFirst();

                if (mealOptional.isPresent()) {
                    meal = mealOptional.get();
                    if (isClientEligibleToOrder(meal, moneyLeft, client.getMoney())) {

                        order.addMeal(meal);
                        menu.removeMeal(meal);
                        countNumberOfProductsOrdered++;
                        moneyLeft = moneyLeft.subtract(meal.getPrice());
                        LOGGER.info("Client with name " + client.getName() + " has ordered a meal " + meal.getName() + " " + meal.getMealType() + " with price: " + meal.getPrice() + " and has " + moneyLeft + " BGN left.");

                    } else {
                        break;
                    }
                }
            } else {
                Optional<Drink> drinkOptional = menu.getDrinks()
                        .stream()
                        .filter(m -> m.isAlcohol() == false)
                        .findFirst();

                if (drinkOptional.isPresent()) {
                    drink = drinkOptional.get();
                    if (isClientEligibleToOrder(drink, moneyLeft, client.getMoney())) {
                        order.addDrink(drink);
                        menu.removeDrink(drink);
                        countNumberOfProductsOrdered++;
                        moneyLeft = moneyLeft.subtract(drink.getPrice());
                        LOGGER.info("Client with name " + client.getName() + " has ordered a meal " + drink.getName() + " with price: " + drink.getPrice() + " and has " + moneyLeft + " BGN left.");

                    } else {
                        break;
                    }
                }
            }
        }

        if (!order.getDrinks().isEmpty() || !order.getMeals().isEmpty()) {
            waiter.addOrder(order);
            LOGGER.info("Waiter " + waiter.getName() + " took the order for client " + client.getName());
        }


    }


    private void makeStudentAndOrdinaryOrder(final Client client, final Menu menu, final Waiter waiter) {
        final Order order = new Order(client);
        Meal meal;
        Drink drink;
        BigDecimal moneyLeft = client.getMoney();
        int countNumberOfProductsOrdered = 0;
        // get random meal from menu meals
        while (countNumberOfProductsOrdered <= MAX_NUMBER_OF_PRODUCTS_TO_ORDER && client.getMoney().subtract(moneyLeft).divide(client.getMoney(), 1, RoundingMode.HALF_EVEN).compareTo(new BigDecimal(MAX_PERCENTAGE_OF_ALLOW_ORDER_COST)) == -1) {

            if (getRandomBoolean()) {

                Optional<Meal> mealOptional = menu.getMeals()
                        .stream()
                        .filter(m -> m.getMealType().equals(getRandomMeal()))
                        .findFirst();

                if (mealOptional.isPresent()) {
                    meal = mealOptional.get();
                    if (isClientEligibleToOrder(meal, moneyLeft, client.getMoney())) {

                        order.addMeal(meal);
                        menu.removeMeal(meal);
                        countNumberOfProductsOrdered++;

                        moneyLeft = moneyLeft.subtract(meal.getPrice());
                        LOGGER.info("Client with name " + client.getName() + " has ordered a meal " + meal.getName() + " " + meal.getMealType() + " with price: " + meal.getPrice() + " and has " + moneyLeft + " BGN left.");

                    } else {
                        break;
                    }
                }
            } else {
                Optional<Drink> drinkOptional = menu.getDrinks()
                        .stream()
                        .filter(m -> m.isAlcohol() == getRandomBoolean())
                        .findFirst();

                if (drinkOptional.isPresent()) {
                    drink = drinkOptional.get();
                    if (isClientEligibleToOrder(drink, moneyLeft, client.getMoney())) {

                        order.addDrink(drink);
                        menu.removeDrink(drink);
                        countNumberOfProductsOrdered++;
                        moneyLeft = moneyLeft.subtract(drink.getPrice());
                        LOGGER.info("Client with name " + client.getName() + " has ordered a meal " + drink.getName() + " with price: " + drink.getPrice() + " and has " + moneyLeft + " BGN left.");

                    } else {
                        break;
                    }
                }
            }
        }
        if (!order.getDrinks().isEmpty() || !order.getMeals().isEmpty()) {
            waiter.addOrder(order);
            LOGGER.info("Waiter " + waiter.getName() + " took the order for client " + client.getName());
        }

    }


    private void makeOrder(final Client cus, final Restaurant res, final Waiter waiter) {


        if (cus instanceof Vegan) {

            makeVeganOrder(cus, res.getMenu(), waiter);
        } else {
            makeStudentAndOrdinaryOrder(cus, res.getMenu(), waiter);
        }
    }

    private void payTheBill(final Client client, final Restaurant res) {

        final Waiter waiter = getWaiterForClient(client, res.getAllWaiters());
        if (waiter != null) {
            final Order order = getOrderByClient(client, waiter);
            final BigDecimal tip = client.getMoney().subtract(order.getPrice());
            waiter.addMoney(tip);
            res.addMoney(order.getPrice());
            LOGGER.info("The restaurant have " + res.getMoney() + " BGN.");
        }
    }

    private Waiter getWaiterForClient(final Client client, final List<Waiter> waiters) {
        Waiter waiter = null;
        for (Waiter w : waiters) {
            for (Order o : w.getOrders()) {
                if (o.getClient().equals(client)) {
                    waiter = w;
                    break;
                }
            }
        }
        return waiter;
    }

    private Order getOrderByClient(final Client client, final Waiter waiter) {
        Order order = null;

        for (Order o : waiter.getOrders()) {
            if (o.getClient().equals(client)) {
                order = o;
                break;
            }
        }
        return order;
    }

    private void askTheBill(final Client client, final Restaurant res) throws IOException {
        final Waiter waiter = getWaiterForClient(client, res.getAllWaiters());
        if (waiter != null) {
            final Order order = getOrderByClient(client, waiter);

            StringBuilder sb = new StringBuilder();
            order.calculateThePrice();
            sb.append("The bill for client: " + client.getName());
            sb.append(System.getProperty("line.separator"));
            for (Meal m : order.getMeals()) {
                sb.append(m.getName() + " - ");

                sb.append(m.getPrice() + " BGN " + System.getProperty("line.separator"));

            }
            for (Drink d : order.getDrinks()) {
                sb.append(d.getName() + " - ");

                sb.append(d.getPrice() + " BGN " + System.getProperty("line.separator"));

            }
            sb.append("Total Price: " + order.getPrice() + System.getProperty("line.separator"));
            sb.append("Waiter: " + waiter.getName());
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/output/bill_" + client.getName() + ".txt"));
            writer.write(sb.toString());

            writer.close();

        }
    }

    private MealType getRandomMeal() {
        Random random = new Random();
        return MealType.values()[random.nextInt(MealType.values().length)];
    }

    private boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

    private Waiter getRandomWaiter(final List<Waiter> waiters) {
        Random rand = new Random();
        return waiters.get(rand.nextInt(waiters.size()));
    }

    private boolean isClientEligibleToOrder(final StockPriceName product, final BigDecimal moneyLeft, final BigDecimal clientMoney) {
        return moneyLeft.subtract(product.getPrice()).compareTo(BigDecimal.ZERO) > 0 && moneyLeft.subtract(product.getPrice()).divide(clientMoney, 1, RoundingMode.HALF_EVEN).compareTo(new BigDecimal(MAX_PERCENTAGE_OF_ALLOW_ORDER_COST)) == -1;
    }


}
